;;; dangermuse-utils.el --- My personal utility functions

;;; Commentary:

;;; Code:

(defun dangermuse/comma-join (l)
  "Join list L into a comma separated string."
  (string-join l ", "))

(provide 'dangermuse-utils)

;;; dangermuse-utils.el ends here

