(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; straight use-package stuff
(setq straight-use-package-by-default t)
(straight-use-package 'use-package)
(use-package git) ;; ensure we can install from git sources

(use-package org)
(org-babel-load-file "~/.emacs.d/emacs-config.org")
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-show-quick-access t nil nil "Customized with use-package company")
 '(display-line-numbers-type 'relative)
 '(display-time-mode t)
 '(global-display-line-numbers-mode t)
 '(inhibit-startup-screen t)
 '(safe-local-variable-values
   '((deft-directory . org-roam-directory)
     (deft-directory org-roam-directory)
     (org-roam-db-location . "./org-roam.db")
     (org-roam-directory . "./")
     (org-roam-directory expand-file-name "./")
     (org-roam-db-location expand-file-name "./org-roam.db")
     (org-roam-directory expand-file-name "."))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "#2d3743" :foreground "#e1e1e0" :inverse-video nil :box nil :strike-through nil :extend nil :overline nil :underline nil :slant normal :weight normal :height 140 :width normal :foundry "nil" :family "JetBrains Mono")))))

;; Computer names needs to go here because set-frame-font does not
;; trigger on startup otherwise
(defvar my/macmini-p (equal (system-name) "dms-Mac-mini.localdomain"))
(if my/macmini-p (set-frame-font
                  "JetBrains Mono 18" nil t))
