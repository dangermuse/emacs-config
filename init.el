(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)
(use-package straight
  :custom
  (straight-use-package-by-default t))
(use-package org)


(setq vc-follow-symlinks nil)

(use-package ef-themes)
(load-theme 'ef-maris-dark :no-confirm)

(setq inhibit-splash-screen 1)

(tool-bar-mode -1)

;; Backups
(setq backup-directory-alist '(("." . "~/.emacs.d/backup"))
      backup-by-copying t   
      version-control t     
      delete-old-versions t 
      kept-new-versions 20  
      kept-old-versions 5)

;; Completion
(use-package vertico
  :custom
  (vertico-cycle t)
  :init
  (vertico-mode))

(use-package savehist
  :init
  (savehist-mode))

(use-package consult)

(use-package marginalia
  :custom
  (marginalia-annotators
  '(marginalia-annotators-heavy marginalia-annotators-light nil))
  :init
  (marginalia-mode))

(use-package orderless
  :init
  (setq completion-styles '(orderless)
        completion-category-defaults nil
        completion-category-overrides '((file (styles . (partial-completion))))))

(use-package swiper)
(use-package which-key
  :init (which-key-mode)
  :config
  (setq which-key-idle-delay 0.5))

(use-package corfu
    :custom
    (corfu-cycle t)                ;; Enable cycling for `corfu-next/previous'
    (corfu-auto nil)               ;; Disable autocompletion
    (corfu-commit-predicate nil)   ;; Do not commit selected candidates on next input
    (corfu-quit-at-boundary t)     ;; Automatically quit at word boundary
    (corfu-quit-no-match t)        ;; Automatically quit if there is no match
    (corfu-preview-current nil)    ;; Disable current candidate preview
    (corfu-preselect-first nil)    ;; Disable candidate preselection
    (corfu-echo-documentation nil) ;; Disable documentation in the echo area
    (corfu-scroll-margin 5)        ;; Use scroll margin
    :init
    (global-corfu-mode))

(use-package dabbrev
  :bind (("M-/" . dabbrev-completion)
         ("C-M-/" . dabbrev-expand)))

(use-package emacs
  :init
  (setq completion-cycle-threshold 3)
  (setq tab-always-indent 'complete))

(add-hook 'eshell-mode-hook
          (lambda ()
            (setq-local corfu-quit-at-boundary t
                        corfu-quit-no-match t
                        corfu-auto nil)
              (corfu-mode)))
(use-package cape)

;; Silence the pcomplete capf, no errors or messages!
(advice-add 'pcomplete-completions-at-point :around #'cape-wrap-silent)

;; Ensure that pcomplete does not write to the buffer
;; and behaves as a pure `completion-at-point-function'.
(advice-add 'pcomplete-completions-at-point :around #'cape-wrap-purify)

(use-package pcmpl-args)

;; Use shell exec path
(use-package exec-path-from-shell)
(exec-path-from-shell-initialize)

;; Disable ctrl-Z
(setq global-unset-key (kbd "C-z"))

;; Quality of life
(use-package crux)
(global-set-key [remap move-beginning-of-line] #'crux-move-beginning-of-line)
(global-set-key [(shift return)] #'crux-smart-open-line)
(global-set-key [remap kill-whole-line] #'crux-kill-whole-line)
(global-set-key (kbd "C-s") 'swiper)

(use-package browse-kill-ring)

(use-package expand-region
  :bind ("C-=" . er/expand-region))

(global-set-key (kbd "C-h f") #'helpful-callable)

(global-set-key (kbd "C-h v") #'helpful-variable)
(global-set-key (kbd "C-h k") #'helpful-key)
(global-set-key (kbd "C-c C-d") #'helpful-at-point)
(global-set-key (kbd "C-c C-d") #'helpful-at-point)
(global-set-key (kbd "C-h C") #'helpful-command)

(setq view-read-only t)

;; reading

(when (eq system-type 'gnu/linux)
  (use-package pdf-tools
    :mode  ("\\.pdf\\'" . pdf-view-mode)
    :config
    (setq-default pdf-view-display-size 'fit-page)
    (setq pdf-annot-activate-created-annotations t)
    (pdf-tools-install :no-query)
    (require 'pdf-occur)))

(use-package esxml
  :straight (esxml :type git :host nil :repo "https://github.com/tali713/esxml.git"))


(use-package focus)
(add-to-list 'focus-mode-to-thing '(nov-mode . paragraph))

(use-package nov
  :after esxml
  :straight (nov :type git :host nil :repo "https://depp.brause.cc/nov.el.git")
  :mode ("\\.epub\\'" . nov-mode))

(defun my-nov-font-setup ()
  (face-remap-add-relative 'variable-pitch :family "Liberation Serif"
                           :height 1.0))
(add-hook 'nov-mode-hook 'my-nov-font-setup)
(add-hook 'nov-mode-hook 'focus-mode)

(setq nov-text-width 80)

;; linting

(use-package flycheck)

;; minibuffers

(setq enable-recursive-minibuffers t)
(minibuffer-depth-indicate-mode 1)

;; Display whitespace
(use-package whitespace
  :custom
  (whitespace-style
   '(face tabs newline trailing tab-mark space-before-tab space-after-tab))
  :config
  (global-whitespace-mode 1))
;; 
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

;; Pollen
(use-package pollen-mode)

;; Eww stuff
(setq shr-cookie-policy nil)
(setq url-cookie-untrusted-urls '(".*"))

;; (use-package org
;;   :init
;;   (setq org-export-allow-bind-keywords t))

(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

;; For org babel templates
(require 'org-tempo)
(add-to-list 'org-structure-template-alist '("sh" . "src shell"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))

;; active Babel languages
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . T)
   (plantuml . T)))

(setq org-hide-emphasis-markers t)

(use-package denote)
(setq denote-save-buffers nil)
(setq denote-known-keywords '("emacs-lisp" "bash" "mesopotamia" "rpg-prep" "ikons" "blood-oath"))
(setq denote-infer-keywords t)
(setq denote-sort-keywords t)
(setq denote-file-type nil) ; Org is the default, set others here
(setq denote-prompts '(title keywords))
(setq denote-excluded-directories-regexp nil)
(setq denote-excluded-keywords-regexp nil)
(setq denote-rename-confirmations '(rewrite-front-matter modify-file-name))

(use-package tex :straight auctex)

;; fix for "Error in post-command-hook (vertico--exhibit): (cyclic-function-indirection ConTeXt-mode)"
(fmakunbound 'ConTeXt-mode)

(setq-default TeX-engine 'xetex)
;;(setq org-agenda-files (list "~/src/personal/events.org"))

;; Use pdf-tools to open PDF files
(setq TeX-view-program-selection '((output-pdf "PDF Tools"))
      TeX-source-correlate-start-server t)

;; Update PDF buffers after successful LaTeX runs
(add-hook 'TeX-after-compilation-finished-functions
          #'TeX-revert-document-buffer)

(use-package ox-tufte)

(use-package plantuml-mode
  :init
  (setq plantuml-default-exec-mode 'jar)
  (setq plantuml-jar-path "/usr/share/java/plantuml.jar")
  (setq org-plantuml-jar-path (expand-file-name "/usr/share/java/plantuml.jar"))
  (setq org-startup-with-inline-images t)
  (add-to-list 'org-src-lang-modes '("plantuml" . plantuml))
  (org-babel-do-load-languages 'org-babel-load-languages '((plantuml . t))))

(use-package markdown-mode
  :commands (markdown-mode gfm-mode)	;
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

;; gnuplot
(use-package gnuplot-mode)
(use-package gnuplot)


;; magit

(use-package magit)
(add-hook 'shell-mode-hook  'with-editor-export-editor)
(add-hook 'term-exec-hook   'with-editor-export-editor)
(add-hook 'eshell-mode-hook 'with-editor-export-editor)

;; niceties

(global-hl-line-mode +1)

(global-display-fill-column-indicator-mode)

(setq-default indent-tabs-mode nil)
(setq-default tab-width 2)

(add-hook 'emacs-lisp-mode-hook #'flycheck-mode)

(use-package helpful
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . helpful-function)
  ([remap describe-symbol] . helpful-symbol)
  ([remap describe-variable] . helpful-variable)
  ([remap describe-command] . helpful-command)
  ([remap describe-key] . helpful-key))

(global-set-key (kbd "C-c C-d") #'helpful-at-point)
(global-set-key (kbd "C-h F") #'helpful-function)
(global-set-key (kbd "C-h C") #'helpful-command)

;; yasnippet
(use-package yasnippet
  :init (yas-global-mode 1))
(use-package yasnippet-snippets)
(use-package clojure-mode)
(use-package cider)
(add-hook 'cider-repl-mode-hook #'cider-company-enable-fuzzy-completion)
(add-hook 'cider-mode-hook #'cider-company-enable-fuzzy-completion)
(use-package parinfer)
;;(use-package paredit)

;; limit eldoc minibugger messages for eglot
(setq eldoc-echo-area-use-multiline-p nil)

;; racket
(use-package racket-mode
  :mode "\\.rkt\\'"
  :hook (racket-mode . racket-xp-mode))

;; sly + sbcl
(setq inferior-lisp-program "sbcl")
(use-package sly)

;; geiser + guile

(use-package geiser)
(use-package geiser-guile)

;; emacs lisp
(use-package suggest)
(use-package aggressive-indent)
(add-hook 'emacs-lisp-mode-hook #'aggressive-indent-mode)

(use-package highlight-quoted)

(use-package elisp-demos)
(advice-add 'helpful-update :after #'elisp-demos-advice-helpful-update)

(setq-local buffer-save-without-query t)

;; environment variables
(setenv "PAGER" "cat")
(setenv "GUILE_LOAD_COMPILED_PATH" "/home/user/.guix-profile/lib/guile/3.0/site-ccache:/home/user/.guix-profile/share/guile/site/3.0")
(setenv "GUILE_LOAD_PATH" "/home/user/.guix-profile/share/guile/site/3.0")
(setenv "GUIX_LOCPATH" "/home/user/.guix-profile/lib/locale")

;; eshell
(use-package eshell
:init
(setq eshell-scroll-to-bottom-on-input 'all
      eshell-error-if-no-glob t
      eshell-hist-ignoredups t
      eshell-save-history-on-exit t
      eshell-prefer-lisp-functions nil
      eshell-destroy-buffer-when-process-dies t
      eshell-cmpl-cycle-completions nil)
(add-hook 'eshell-mode-hook
(lambda ()
  (add-to-list 'eshell-visual-commands "ssh")
  (add-to-list 'eshell-visual-commands "tail")
  (add-to-list 'eshell-visual-commands "top")
  (eshell/alias "ff" "find-file $1")
  (eshell/alias "ffow" "find-file-other-window $1")
  (eshell/alias "fd" "find-dired $PWD \"\"")
  (eshell/alias "dired-ssh" (format "dired /sshx:%s" "$1"))
  (eshell/alias "d" "dired $1"))))

(use-package elisp-refs)
  
(setq eshell-ls-initial-args '("-alth"))

(add-to-list 'eshell-modules-list 'eshell-tramp)
(setq eshell-prefer-lisp-functions t
      eshell-prefer-lisp-variables t)
(setq password-cache t
password-cache-expiry 30)

(when (and (executable-find "fish")
           (use-package fish-completion
             :hook (eshell-mode . fish-completion-mode))))

;; emacsql
(use-package emacsql)
(use-package emacsql-sqlite)

;; ace stuff

(use-package ace-popup-menu)
(ace-popup-menu-mode 1)

(use-package ace-window)

(global-set-key (kbd "M-o") 'ace-window)
(setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))


;; programming

;; shortcuts

(defun dm/eshell-new()
  "Open a new instance of eshell."
  (interactive)
  (eshell 'N))
(defun dm/eww-new ()
  "Open a new instance of eww"
  (interactive)
  (let ((url (read-from-minibuffer "Enter URL or keywords: ")))
  (switch-to-buffer (generate-new-buffer "eww"))
  (eww-mode)
  (eww url)))
(global-set-key (kbd "s-b") 'dm/eww-new)
(global-set-key (kbd "s-<return>") 'dm/eshell-new)
(global-set-key (kbd "C-c M-<return>") 'dm/eshell-new)
(global-set-key (kbd "C-c M-b") 'dm/eww-new)2

(use-package all-the-icons)

(setq inhibit-compacting-font-caches t)

;; modeline
;;(use-package mood-line)
;;(mood-line-mode)

;; Display 80 column marker
(global-display-fill-column-indicator-mode)

(display-battery-mode 1)
(display-time-mode 1)

;; For long lines in eshell, may or may not help
(setq bidi-paragraph-direction 'left-to-right)

;; (custom-set-faces
;;  ;; custom-set-faces was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(default ((t (:family "JetBrains Mono" :foundry "JB" :slant normal :weight regular :height 122 :width normal)))))
;; (custom-set-variables
;;  ;; custom-set-variables was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(display-battery-mode t)
;;  '(display-time-mode t)
;;  '(tool-bar-mode nil))

;; (load "~/.emacs.d/blog-config.el")
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("dc2e1b0abb9a5d2033f6d881618932dcdb9af7633d8fa44336f9c9a3484379bd" "184d32f815d68d6aadf7dde7d4f7b8d8059fdd177630e501a4a52989a568d785" "6cff32351bcb1726accf9dcf9c400367971eaa8bb1d163409b78ea9c9a6ae8d0" default)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'downcase-region 'disabled nil)

;; load my code
(add-to-list 'load-path (expand-file-name "~/.emacs.d/melisp"))
