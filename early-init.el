(setq package-enable-at-startup nil)

(add-to-list 'default-frame-alist '(font . "JetBrains Mono-12"))

(add-to-list 'default-frame-alist '(line-spacing . 0.2))
